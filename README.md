# parallel programming tasks

## Task 1
Register, login by c language without gui ..
Only input, output

## Task 2
A user registration and login system using a union.  
During registration, users set an "active" flag to true/1 or false/0.  
The login process checks this flag, allowing access only if it's true/1.

