
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#define MAX_USERS 100
#define MAX_USERNAME_LENGTH 20
#define MAX_EMAIL_LENGTH 50
#define MAX_PASSWORD_LENGTH 20

struct User {
    char username[MAX_USERNAME_LENGTH];
    char email[MAX_EMAIL_LENGTH];
    char password[MAX_PASSWORD_LENGTH];
};

struct User users[MAX_USERS];
int userCount = 0;

bool isUserExist(const char* email, const char* password) {
    for (int i = 0; i < userCount; i++) {
        if ( strcmp(users[i].email, email) == 0 &&
            strcmp(users[i].password, password) == 0) {
            return true;
        }
    }
    return false;
}

void registerUser() {
    if (userCount < MAX_USERS) {
        printf("Enter a username: ");
        scanf("%s", users[userCount].username);
        printf("Enter your email: ");
        scanf("%s", users[userCount].email);

        while (true) {
            printf("Enter a password: ");
            scanf("%s", users[userCount].password);
            printf("Re-enter the password to confirm: ");
            char confirmPassword[MAX_PASSWORD_LENGTH];
            scanf("%s", confirmPassword);
            
            if (strcmp(users[userCount].password, confirmPassword) == 0) {
                break;
            } else {
                printf("Passwords do not match. Please try again.\n");
            }
        }
        
        userCount++;
        printf("Registration successful!\n");
    } else {
        printf("User limit reached. Cannot register more users.\n");
    }
}

int main() {
    int choice;
    char username[MAX_USERNAME_LENGTH];
    char email[MAX_EMAIL_LENGTH];
    char password[MAX_PASSWORD_LENGTH];

    do {
        printf("\n1. Register\n2. Login\n3. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);

        switch (choice) {
            case 1:
                registerUser();
                break;
            case 2:
                printf("Enter your email: ");
                scanf("%s", email);
                printf("Enter your password: ");
                scanf("%s", password);
                if (isUserExist( email, password)) {
                    printf("Login successful! Welcome, %s.\n", email);
                } else {
                    printf("Invalid username, email, or password. Please try again.\n");
                }
                break;
            case 3:
                printf("Goodbye!\n");
                break;
            default:
                printf("Invalid choice. Please try again.\n");
        }
    } while (choice != 3);

    return 0;
}

